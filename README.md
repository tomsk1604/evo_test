# evo_test

Evo_test is a Django DRF backend application to work with user diaries. Every Diary may have several text Notes.
It is written in Python 3. 
 
## Model description
### Diary Model:
* title: string (required field)
* expiration: Date after which you can delete the diary. Can only be assigned to private diaries
* kind: Diary type. Values: public, private (required field)
* user: Associated with user one-to-many. The user has many diaries, the diary has
one user.

### Note Model:
* text: text field.
* linked to Diary one-to-many. The diary has many notes, the note has one
a diary
  
### Model User
* login:string. User login in the form of email (required field)
* password: user password (required field)

## Description of functionality
* Implemented CRUD for Diary, Note models
* All users can view diaries and pages of all users, but the user can only manage his diaries and pages
* To work with API of Diary and Note, you need to log in. 
* Data transfer format - JSON
* Background task deletes old diaries every 10 minutes.
* Diaries and Notes rotes have pagination and ability to search, filter and order    
* Implemented API auto-generation in swagger

##Technologies and Libraries
* django
* celery
* redis
* postgres
* docker
* pytest

## How to build from source

### Prerequisites
* [Python 3.8+][python]
* Django 4.1
* DRF 3.13.1

### Building

    $ cd <evo_test directory>
    $ python3 -m venv ./env
    $ source ./env/bin/activate
    $ pip install -r requirements.txt
    

## Running tests
    ...

[python]: http://www.python.org/
