import os
from celery import Celery
 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'evo_test.settings')
 
app = Celery('diary')
app.config_from_object('django.conf:settings')
 
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'delete-diaries-every-10-minutes': {
        'task': 'diary.tasks.delete_expired_diaries',
        'schedule': 600
    },
}