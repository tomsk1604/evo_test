from django.db import models

from accounts.models import MyUser


class Note(models.Model):
    text = models.TextField(blank=True, null=True)
    diary = models.ForeignKey('Diary', on_delete=models.CASCADE, null=True)
    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id}: D{self.diary_id}: U{self.owner_id}: {self.text[:50]}'

    class Meta:
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'


class Diary(models.Model):
    title = models.CharField(max_length=255)
    expiration = models.DateField(blank=True, null=True)
    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE)

    class DiaryType(models.TextChoices):
        PRIVATE = 'private'
        PUBLIC = 'public'

    kind = models.CharField(
        max_length=7,
        choices=DiaryType.choices,
        default=DiaryType.PUBLIC,
        blank=False,
        null=False
    )

    def __str__(self):
        return f'{self.id} U:{self.owner_id}: {self.title}'

    def save(self, *args, **kwargs):
        if self.kind == self.DiaryType.PUBLIC:
            self.expiration = None
        else:
            pass
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Diary'
        verbose_name_plural = 'Diaries'
