from datetime import date

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from diary.models import Diary
from diary.serializers import DiariesSerializer


class BooksApiTestCase(APITestCase):
    def setUp(self):
        self.diary_1 = Diary.objects.create(title='Test diary 1', expiration=None,
                                            diary_type='public')
        self.diary_2 = Diary.objects.create(title='Test diary 2', expiration=date(2022, 10, 4).isoformat(),
                                            diary_type='public')

    def test_get(self):
        url = reverse('diary-list')
        print(url)
        response = self.client.get(url)
        serializer_data = DiariesSerializer([self.diary_1, self.diary_2], many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data['results'])
