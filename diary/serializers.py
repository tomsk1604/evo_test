from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from diary.models import Diary, Note


class DiariesSerializer(ModelSerializer):
    class Meta:
        model = Diary
        fields = '__all__'

    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())


class NotesSerializer(ModelSerializer):
    class Meta:
        model = Note
        fields = '__all__'

    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())
