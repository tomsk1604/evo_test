from django.contrib import admin
from .models import Diary
from .models import Note


# Register your models here.

@admin.register(Diary)
class DiaryAdmin(admin.ModelAdmin):
    pass


@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    pass
