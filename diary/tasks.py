from datetime import date

from click_repl import repl
from django.urls import reverse
from django.contrib.auth import get_user_model

from diary.models import Diary
from evo_test.celery import app
 
 
@app.task
def delete_expired_diaries():
    try:
        Diary.objects.filter(expiration__lt=date.today()).delete()
    except Exception as e:
        print('Failed to delete expired diaries: ' + repr(e))
