import django_filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

from diary.models import Diary, Note
from diary.permissions import IsOwnerOrStaffOrReadOnly
from diary.serializers import DiariesSerializer, NotesSerializer


class DiaryPagination(PageNumberPagination):
    """
    Specifies a size of pagination for a Diary.
    The default PAGE_SIZE for all views is set in settings.py
    """
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 50


class DiaryViewSet(ModelViewSet):
    """
    This view should return a list of all Diaries
    """
    queryset = Diary.objects.all()
    serializer_class = DiariesSerializer
    permission_classes = [IsOwnerOrStaffOrReadOnly]
    pagination_class = DiaryPagination
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['title', 'expiration', 'kind']
    search_fields = ['title']
    ordering_fields = ['title', 'expiration', 'kind']


class NoteViewSet(ModelViewSet):
    """
    This view should return a list of all Notes
    """
    queryset = Note.objects.all()
    serializer_class = NotesSerializer
    permission_classes = [IsOwnerOrStaffOrReadOnly]

    class NoteFilter(django_filters.FilterSet):
        diary = django_filters.ModelChoiceFilter(field_name='diary',
                                                 queryset=Diary.objects.all())

        class Meta:
            note = Note
            fields = ('diary__id',)

    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_class = NoteFilter
    search_fields = ['text']
    ordering_fields = ['diary']


class DiaryNotesViewSet(NoteViewSet):
    """
    This view should return a list of all Notes by
    the Diary ID passed in the URL
    """
    def get_queryset(self):
        diary = self.kwargs['diary_pk']
        return Note.objects.filter(diary_id=diary)
